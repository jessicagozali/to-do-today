# To Do Today

&nbsp;    

>
> Live website link: [https://todotoday-app.netlify.app/](https://todotoday-app.netlify.app/)
>

&nbsp;    

## Purpose

To Do Today is a simple to do list with a touch of TLC (Tender Loving Care) that will give users information about current weather condition as well as a caring message to remind you that you are loved.

&nbsp;    

---

&nbsp;    

## Features

### Caring Message

A caring message will be picked based on the weather. Some messages will simply remind users how great they are, or remind users to bring an umbrella on a rainy day.

&nbsp;    

### Current Weather

&nbsp;    

![Current Weather and Temperature Information](public/docs/weather.gif)

&nbsp;    

Current weather condition and temperature will be fetched from OpenWeatherMap API based on the selected city. The information would give users a bit of an overview of the weather condition so they can make their plan accordingly.

&nbsp;    

### To Do List

&nbsp;    

![TO Do List Demo](public/docs/todo-demo.gif)

&nbsp;    

A simple to-do list for a quick reminder for the day. Users can add their tasks, cross them out or delete them from the list.

&nbsp;    

## Tech Stack

Framework:

- React JS

Deployment:

- Netlify

Other tools:

- VS Code
- Bitbucket

&nbsp;    

## Acknowledgements

API used in the app belongs and all credits are given to the respective owner.

- [OpenWeatherMap API](https://openweathermap.org/)
