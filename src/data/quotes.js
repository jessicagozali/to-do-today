export const quotes = [
  {
    quote: "Good day, sunshine! Your smile will make my day... :)",
    time: "day",
    weather: "01d"
  },
  {
    quote: "Hello, love... You shine like a star tonight...",
    time: "night",
    weather: "01n"
  },
  {
    quote: "Hiya! Few clouds? No worries! Today's gonna be lovely!",
    time: "day",
    weather: "02d"
  },
  {
    quote: "You're doing well! Be positive, tomorrow has its own worries!",
    time: "night",
    weather: "02n"
  },
  {
    quote: "Today might be cloudy, but it won't cloud your toughts!",
    time: "day",
    weather: "03d"
  },
  {
    quote: "Will there be any stars peeking through the clouds tonight?",
    time: "night",
    weather: "03n"
  },
  {
    quote: "Look up, cupcake! The sun will always shine above the clouds...",
    time: "day",
    weather: "04d"
  },
  {
    quote: "Look, sweetheart! Clouds are the sky's imagination...",
    time: "night",
    weather: "04n"
  },
  {
    quote: "You are wise to bring an umbrella today, they said it's going to shower...",
    time: "day",
    weather: "09d"
  },
  {
    quote: "Showers won't stop your plan tonight, because you have an umbrella! Ha!",
    time: "night",
    weather: "09n"
  },
  {
    quote: "Don't forget your umbrella, dear! Stay dry!",
    time: "day",
    weather: "10d"
  },
  {
    quote: "Have you got your umbrella? And a jacket? In case it's going to be cold tonight...",
    time: "night",
    weather: "10n"
  },
  {
    quote: "The weather might not be pleasant today, so seek shelther when it looks like it's going to be bad...",
    time: "day",
    weather: "11d"
  },
  {
    quote: "Avoid going outdoor tonight, honey! Let's just stay indoor and cuddle!",
    time: "night",
    weather: "11n"
  },
  {
    quote: "The snow is coming! Bundle up and stay warm, darling!",
    time: "day",
    weather: "12d"
  },
  {
    quote: "Tonight is snowing, so please stay warm, dear!",
    time: "night",
    weather: "12n"
  },
  {
    quote: "The visibility today might be low, if you have to drive, please drive carefully!",
    time: "day",
    weather: "12d"
  },
  {
    quote: "Please slow down if you have to drive tonight! Use fog lights if necessary!",
    time: "night",
    weather: "12n"
  }
];
