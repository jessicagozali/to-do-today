export const res = { data:
  {
    coord: {
      lon: 151.2073,
      lat: -33.8679
    },
    weather: [
      {
        id: 500,
        main: "Rain",
        description: "light rain",
        icon: "10d"
      }
    ],
    base: "stations",
    main: {
      temp: 296.86,
      feels_like: 297.58,
      temp_min: 294.84,
      temp_max: 298.01,
      pressure: 1009,
      humidity: 88
    },
    visibility: 10000,
    wind: {
      speed: 2.24,
      deg: 149,
      gust: 4.02
    },
    rain: {
      '1h': 0.14
    },
    clouds: {
      all: 75
    },
    dt: 1641371319,
    sys: {
      type: 2,
      id: 2018875,
      country: "AU",
      sunrise: 1641322230,
      sunset: 1641373785
    },
    timezone: 39600,
    id: 2147714,
    name: "Sydney",
    cod: 200
  }
};
