import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CardHeader from './components/CardHeader';
import Header from './components/Header';
import ListRow from './components/ListRow';

import { pickQuote } from './utils/quotePicker';
import { getFullDate, getDayName } from './utils/dateFormatter';
import { tempKtoC, getWeatherIcon, getCityCode } from './utils/conditionFormatter';
// import { res } from './data/weatherMock';
import WeatherInfo from './components/WeatherInfo';
import Footer from './components/Footer';

function App() {
  const [quote, setQuote] = useState('');
  const [todayInfo, setTodayInfo] = useState({});
  const [currentLocation, setCurrentLocation] = useState("Sydney");
  const { date, dayName } = todayInfo;
  const [clock, setClock] = useState('');
  const [condition, setCondition] = useState({});
  const { temperature, weatherStatus, weatherIcon } = condition;
  const [toDos, setToDos] = useState([]);
  const [newToDo, setNewToDo] = useState({
    status: '',
    content: ''
  });
  const { content } = newToDo;

  useEffect(() => {
    if (currentLocation) {
      axios.get(`https://api.openweathermap.org/data/2.5/weather?id=${getCityCode(currentLocation)}&appid=4dc2e858e0d45338ce6d54102f8827ba`)
      .then((res) => {
        console.log(res.data);

        setQuote(pickQuote(res.data.weather[0].icon));

        setCondition({
          temperature: tempKtoC(res.data.main.temp),
          weatherStatus: res.data.weather.main,
          weatherIcon: getWeatherIcon(res.data.weather[0].icon),
        });
      }).catch((err) => {
        console.error(err);
      });
    }
    
    setTodayInfo({
      date: getFullDate(new Date()),
      dayName: getDayName(new Date())
    });

    setInterval(() => {
      setClock(new Date().toLocaleTimeString());
    }, 1000);
  }, [currentLocation]);

  const handleSubmit = (e) => {
    e.preventDefault();

    setToDos([...toDos, newToDo]);
    setNewToDo({
      status: '',
      content: ''
    });
  };

  const handleChange = (e) => {
    setNewToDo({
      status: "active",
      content: e.target.value
    });
  };

  const handleCrossOut = (item) => {
    setToDos(t => t.map(todo => todo.content === item ? todo.status === "active" ? {...todo, status: "done"} : {...todo, status: "active"} : todo));
  };

  const handleDelete = (item) => {
    setToDos(t => t.filter(todo => todo.content !== item));
  };

  return (
    <>
      <Header quote={quote} />
      <main>
        <div id="card">
          <CardHeader setCurrentLocation={setCurrentLocation} currentLocation={currentLocation} date={date} />

          <WeatherInfo
            dayName={dayName}
            clock={clock}
            currentLocation={currentLocation}
            temperature={temperature}
            weatherIcon={weatherIcon}
            weatherStatus={weatherStatus}
          />

          {/* TO DO LIST */}
          <div id="to-do">
            {toDos.length > 0
              ? toDos.map((item, i) => <ListRow key={i} item={item} handleCrossOut={handleCrossOut} handleDelete={handleDelete} />)
              : <div className="list-row">
                  <div className="col-text">
                    Nothing to do...
                  </div>
                </div>
            }
          </div>

          {/* Add To Do */}
          <div id="add-to-do">
            <form onSubmit={handleSubmit}>
              <input type="text" value={content} onChange={handleChange} placeholder="Add to-do" />
              <button type="submit"><i className="fa-solid fa-plus"></i></button>
            </form>
          </div>
        </div>
      </main>
      
      <Footer />
    </>
  );
}

export default App;
