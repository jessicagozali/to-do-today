import React from 'react';

const Footer = () => {
  return (
    <footer>
      <div>
        <span>To Do Today &copy; {new Date().getFullYear()}. Jessica Gozali.</span>
      </div>
    </footer>
  )
}

export default Footer;
