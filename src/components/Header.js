import React from 'react';

const Header = ({ quote }) => {
  return (
    <header>
      <h1>To Do Today</h1>
      <p>{quote}</p>
    </header>
  )
}

export default Header;
