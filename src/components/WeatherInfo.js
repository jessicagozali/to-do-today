import React from 'react';

const WeatherInfo = ({
  dayName,
  clock,
  temperature,
  weatherIcon,
  weatherStatus
}) => {
  return (
    <div id="weather-info">
      <div className="col-half">
        <h4>{dayName}</h4>
        <div className="time">{clock}</div>
        <h2>{temperature}&#176;</h2>
      </div>
      <div className="col-half">
        <div className="weather-icon"><img src={weatherIcon} alt={weatherStatus} /></div>
        <div className="weather-status">{weatherStatus}</div>
      </div>
    </div>
  )
}

export default WeatherInfo;
