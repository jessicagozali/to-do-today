import React from 'react'

const ListRow = ({ item, handleCrossOut, handleDelete }) => {
  return (
    <div className="list-row">
      <div className="col-action">
        <i
          className={`${item.status === 'done' ? 'fa-solid' : 'fa-regular'} fa-circle`}
          onClick={() => handleCrossOut(item.content)}
        >
        </i>
      </div>
      <div className={`col-text ${item.status === 'done' ? 'done' : ''}`}>
        {item.content}
      </div>
      <div className="col-action">
        <i
          className="fa-regular fa-trash-can"
          onClick={() => handleDelete(item.content)}
        >
        </i>
      </div>
    </div>
  )
}

export default ListRow;
