import React from 'react';

const CardHeader = ({ setCurrentLocation, currentLocation, date }) => {
  const cities = [
    "Sydney",
    "Melbourne",
    "Brisbane",
    "Perth",
    "Adelaide",
    "Canberra",
    "Darwin",
    "Hobart"
  ];

  const handleChange = (e) => {
    setCurrentLocation(e.target.value);
  };

  return (
    <div id="card-header">
      {cities.length > 0 &&
        <form>
          <select
            className="dropdown-select"
            value={currentLocation}
            onChange={handleChange}
          >
            {cities.map((city, i) => (
                city === "Sydney"
                ? <option key={i} value={city} defaultValue>{city}</option>
                : <option key={i} value={city}>{city}</option>
              )
            )}
          </select>
        </form>
      }

      <div className="col-half date">{date}</div>
    </div>
  )
}

export default CardHeader;
