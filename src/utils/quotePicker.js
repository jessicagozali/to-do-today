import { quotes } from '../data/quotes';

const pickQuote = (code) => {
  const qs = quotes.filter(q => q.weather === code);
  return qs[0].quote;
};

export {
  pickQuote
}
