// Days
const nameOfDays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];

// Months
const nameOfMonths = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
];

const getFullDate = (rawDate) => {
  const dateNum = rawDate.getDate();
  const month = nameOfMonths[rawDate.getMonth()];
  const year = rawDate.getFullYear();
  return (`${dateNum} ${month} ${year}`);
};

const getDayName = (rawDate) => {
  const day = nameOfDays[rawDate.getDay()];
  return (`${day}`);
};

export {
  getFullDate,
  getDayName
}
