const tempKtoC = (kDeg) => {
  return (kDeg - 273.15).toFixed(1);
};

const getWeatherIcon = (code) => {
  switch (code) {
    case "01d":
      return "./img/clear-d.png"
    case "01n":
      return "./img/clear-n.png"
    case "02d":
      return "./img/cloud-f-d.png"
    case "02n":
      return "./img/cloud-f-n.png"
    case "03d":
    case "03n":
      return "./img/cloud-s.png"
    case "04d":
    case "04n":
      return "./img/cloud-b.png"
    case "09d":
    case "09n":
      return "./img/shower.png"
    case "10d":
    case "10n":
      return "./img/rain.png"
    case "11d":
    case "11n":
      return "./img/thunderstorm.png"
    case "13d":
    case "13n":
      return "./img/snow.png"
    case "50d":
    case "50n":
      return "./img/mist.png"
    default:
      return "./img/clear-d.png"
  }
};

const getCityCode = (cityName) => {
  switch (cityName) {
    case "Sydney":
      return "2147714";
    case "Melbourne":
      return "2158177";
    case "Brisbane":
      return "2174003";
    case "Perth":
      return "2063523";
    case "Adelaide":
      return "1023656";
    case "Canberra":
      return "2172517";
    case "Darwin":
      return "2073124";
    case "Hobart":
      return "2163355";
    default:
      return ;
  }
};

export {
  tempKtoC,
  getWeatherIcon,
  getCityCode
}
